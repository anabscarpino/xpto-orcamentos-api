﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using Xpto.Aplicacoes.Interfaces;
using Xpto.Entidades;
using Xpto.Models.Requests;
using Xpto.Models.Responses;

namespace Xpto.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrdemServicoController : ControllerBase
    {
        private readonly ILogger<OrdemServicoController> _logger;
        private readonly IOrdemServicoAppServico _ordemServicoAppServico;

        public OrdemServicoController(ILogger<OrdemServicoController> logger, IMapper mapper, IOrdemServicoAppServico ordemServicoAppServico)
        {
            _logger = logger;
            _ordemServicoAppServico = ordemServicoAppServico;
        }

        [HttpPost]
        public ActionResult Post(OrdemServicoRequest request)
        {
            if(request == null)
            {
                throw new ArgumentNullException();
            }
            _ordemServicoAppServico.InserirOrdemServico(request);
            return Ok();
        }

        [HttpGet]
        public ActionResult Get()
        {
            IEnumerable<OrdemServicoResponse> ordensServico = _ordemServicoAppServico.ListarOrdensServico();
            return Ok(ordensServico);
        }

        [HttpGet("{id:int}")]
        public ActionResult Get(int id)
        {
            if(id <= 0)
            {
                throw new ArgumentException();
            }

            OrdemServicoResponse ordemServico = _ordemServicoAppServico.RecuperarOrdemServico(id);
            return Ok(ordemServico);
        }

        [HttpDelete("{id:int}")]
        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                throw new ArgumentException();
            }
            _ordemServicoAppServico.ExluirOrdemServico(id);
            return Ok();
        }

        [HttpPut]
        public ActionResult Put(OrdemServicoRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException();
            }
            _ordemServicoAppServico.EditarOrdemServio(request);
            return Ok();
        }
    }
}
