﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xpto.Entidades;

namespace Xpto.Repositorios
{
    public class XptoContext : DbContext
    {
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<PrestadorServico> PrestadoresServico { get; set; }
        public DbSet<OrdemServico> OrdensServico { get; set; }

        public XptoContext(DbContextOptions<XptoContext> options) :
            base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Cliente>(entity =>
            {
                entity.ToTable("cliente");
                entity.Property(p => p.Nome).HasColumnName("nome");
                entity.Property(p => p.Id).HasColumnName("id");
                entity.Property(p => p.CpfCnpj).HasColumnName("cpf_cnpj");
                entity.Property(p => p.DocumentoInvalido).HasColumnName("documento_invalido");
            });

            builder.Entity<PrestadorServico>(entity =>
            {
                entity.ToTable("prestador_servico");
                entity.Property(p => p.Nome).HasColumnName("nome");
                entity.Property(p => p.Id).HasColumnName("id");
                entity.Property(p => p.CpfCnpj).HasColumnName("cpf_cnpj");
                entity.Property(p => p.DocumentoInvalido).HasColumnName("documento_invalido");
            });

            builder.Entity<OrdemServico>(entity =>
            {
                entity.ToTable("ordem_servico");
                entity.Property(p => p.Id).HasColumnName("id");
                entity.Property(p => p.Numero).HasColumnName("numero");
                entity.Property(p => p.Titulo).HasColumnName("titulo");
                entity.Property(p => p.DataExecucao).HasColumnName("data_execucao");
                entity.Property(p => p.Valor).HasColumnName("valor");
                entity.Property(p => p.ClienteId).HasColumnName("cliente_id");
                entity.Property(p => p.PrestadorServicoId).HasColumnName("prestador_servico_id");
            });
        }
    }
}
