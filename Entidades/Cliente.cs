﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Xpto.Entidades
{
    [Table("cliente")]
    public class Cliente
    {
        [Key]
        public int Id { get; set; }
        public string CpfCnpj { get; set; }
        public string Nome { get; set; }
        public bool DocumentoInvalido { get; set; }
    }
}
