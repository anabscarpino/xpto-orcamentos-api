﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Xpto.Entidades
{
    [Table("ordem_servico")]
    public class OrdemServico
    {
        [Key]
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Titulo { get; set; }
        public DateTime DataExecucao { get; set; }
        public decimal Valor { get; set; }

        [ForeignKey("ClienteId")]
        public int ClienteId { get; set; }
        public Cliente Cliente { get; set; }

        [ForeignKey("PrestadorServicoId")]
        public int PrestadorServicoId { get; set; }
        public PrestadorServico PrestadorServico { get; set; }
    }
}
