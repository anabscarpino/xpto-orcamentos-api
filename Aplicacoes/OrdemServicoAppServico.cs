﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xpto.Aplicacoes.Interfaces;
using Xpto.Entidades;
using Xpto.Models.Requests;
using Xpto.Models.Responses;
using Xpto.Servicos.Interfaces;

namespace Xpto.Aplicacoes
{
    public class OrdemServicoAppServico : IOrdemServicoAppServico
    {
        private readonly IClienteServico _clienteServico;
        private readonly IOrdemServicoServico _ordemServicoServico;
        private readonly IPrestadorServicoServico _prestadorServicoServico;
        private readonly IMapper _mapper;


        public OrdemServicoAppServico(IClienteServico clienteServico, IOrdemServicoServico ordemServicoServico, IPrestadorServicoServico prestadorServicoServico, IMapper mapper)
        {
            _clienteServico = clienteServico;
            _prestadorServicoServico = prestadorServicoServico;
            _ordemServicoServico = ordemServicoServico;
            _mapper = mapper;
        }

        public void InserirOrdemServico(OrdemServicoRequest request)
        {
            if(!String.IsNullOrEmpty(request.Numero))
            {
                OrdemServico existente = _ordemServicoServico.RecuperarPorNumero(request.Numero);
                if(existente != null) 
                {
                    throw new ArgumentException("Número de OS já existente.");
                }

                Cliente cliente = _clienteServico.Recuperar(request.CnpjCliente);
                if (cliente == null)
                {
                    cliente = new Cliente();
                    cliente.CpfCnpj = request.CnpjCliente;
                    cliente.Nome = request.NomeCliente;
                    cliente.DocumentoInvalido = false;
                    cliente = _clienteServico.Inserir(cliente);
                }

                PrestadorServico prestadorServico = _prestadorServicoServico.Recuperar(request.CpfPrestadorServico);
                if (prestadorServico == null)
                {
                    prestadorServico = new PrestadorServico();
                    prestadorServico.CpfCnpj = request.CpfPrestadorServico;
                    prestadorServico.Nome = request.NomePrestadorServico;
                    prestadorServico.DocumentoInvalido = false;
                    prestadorServico = _prestadorServicoServico.Inserir(prestadorServico);
                }

                OrdemServico nova = _mapper.Map<OrdemServico>(request);
                nova.ClienteId = cliente.Id;
                nova.PrestadorServicoId = prestadorServico.Id;

                _ordemServicoServico.Inserir(nova);
            }
            else
            {
                throw new ArgumentNullException("Número de OS inválido!");
            }

        }

        public void EditarOrdemServio(OrdemServicoRequest request)
        {
            OrdemServico ordemServico;
            if (request.Id != null) 
            {
                ordemServico = _ordemServicoServico.Recuperar((int)request.Id);

            }
            else
            {
                ordemServico = _ordemServicoServico.RecuperarPorNumero(request.Numero);
            }

            if (ordemServico == null)
            {
                throw new ArgumentException("Ordem de serviço não encontrada.");
            }

            Cliente cliente = _clienteServico.Recuperar(request.CnpjCliente);
            if(cliente != null) 
            {
                if (cliente.Id != ordemServico.ClienteId)
                {
                    ordemServico.ClienteId = cliente.Id;
                }
            }
            else
            {
                cliente = new Cliente();
                cliente.Nome = request.NomeCliente;
                cliente.CpfCnpj = request.CnpjCliente;
                cliente.DocumentoInvalido = false;
                cliente = _clienteServico.Inserir(cliente);

                ordemServico.ClienteId = cliente.Id;
            }

            PrestadorServico prestadorServico = _prestadorServicoServico.Recuperar(request.CpfPrestadorServico);
            if (prestadorServico != null)
            {
                if (prestadorServico.Id != ordemServico.PrestadorServicoId)
                {
                    ordemServico.PrestadorServicoId = prestadorServico.Id;
                }
            }
            else
            {
                prestadorServico = new PrestadorServico();
                prestadorServico.Nome = request.NomePrestadorServico;
                prestadorServico.CpfCnpj = request.CpfPrestadorServico;
                prestadorServico.DocumentoInvalido = false;
                prestadorServico = _prestadorServicoServico.Inserir(prestadorServico);

                ordemServico.ClienteId = cliente.Id;
            }

            ordemServico.Titulo = request.Titulo;
            ordemServico.DataExecucao = request.DataExecucao;
            ordemServico.Valor = request.Valor;
            _ordemServicoServico.Editar(ordemServico);
        }

        public void ExluirOrdemServico(int id)
        {
            OrdemServico ordemServico = _ordemServicoServico.Recuperar(id);
            if(ordemServico == null)
            {
                throw new ArgumentException("Ordem de serviço não encontrada.");
            }

            _ordemServicoServico.Excluir(id);
        }

        public OrdemServicoResponse RecuperarOrdemServico (int id) 
        {
            OrdemServico ordemServico = _ordemServicoServico.Recuperar(id);
            if(ordemServico == null)
            {
                throw new ArgumentException("Ordem de serviço não encontrada.");
            }
            return _mapper.Map<OrdemServicoResponse>(ordemServico);
        }

        public IEnumerable<OrdemServicoResponse> ListarOrdensServico()
        {
            IEnumerable<OrdemServico> ordens = _ordemServicoServico.Listar();
            return _mapper.Map<IEnumerable<OrdemServicoResponse>>(ordens);
        }
    }
}
