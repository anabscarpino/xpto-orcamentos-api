﻿using System.Collections.Generic;
using Xpto.Models.Requests;
using Xpto.Models.Responses;

namespace Xpto.Aplicacoes.Interfaces
{
    public interface IOrdemServicoAppServico
    {
        void InserirOrdemServico(OrdemServicoRequest request);
        void EditarOrdemServio(OrdemServicoRequest request);
        void ExluirOrdemServico(int id);
        OrdemServicoResponse RecuperarOrdemServico(int id);
        IEnumerable<OrdemServicoResponse> ListarOrdensServico();

    }
}
