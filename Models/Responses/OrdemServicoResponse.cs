﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Xpto.Models.Responses
{
    public class OrdemServicoResponse
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Titulo { get; set; }
        public string NomeCliente { get; set; }
        public string CnpjCliente { get; set; }
        public string CpfPrestadorServico { get; set; }
        public string NomePrestadorServico { get; set; }
        public DateTime DataExecucao { get; set; }
        public decimal Valor { get; set; }
    }
}
