﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xpto.Entidades;
using Xpto.Repositorios;
using Xpto.Servicos.Interfaces;

namespace Xpto.Servicos
{
    public class ClienteServico : IClienteServico
    {
        private readonly XptoContext _xptoContext;

        public ClienteServico(XptoContext xptoContext)
        {
            _xptoContext = xptoContext;
        }

        public Cliente Inserir(Cliente cliente)
        {
            if (!String.IsNullOrEmpty(cliente.Nome)) { cliente.Nome.ToLower(); }
            if (!String.IsNullOrEmpty(cliente.CpfCnpj)) { cliente.CpfCnpj = cliente.CpfCnpj.Replace(".", "").Replace("-", "").Replace("/", ""); }

            if (!CnpjValido(cliente.CpfCnpj))
            {
                cliente.DocumentoInvalido = true;
            }

            _xptoContext.Clientes.Add(cliente);
            _xptoContext.SaveChanges();
            return cliente;
        }

        public Cliente Recuperar(string cpf)
        {
            return _xptoContext.Clientes.FirstOrDefault(x => x.CpfCnpj == cpf);
        }
        private bool CnpjValido(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;

            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();

            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }
    }
}
