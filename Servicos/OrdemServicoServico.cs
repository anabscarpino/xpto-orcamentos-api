﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using Xpto.Entidades;
using Xpto.Repositorios;
using Xpto.Servicos.Interfaces;

namespace Xpto.Servicos
{
    public class OrdemServicoServico : IOrdemServicoServico
    {
        private readonly XptoContext _xptoContext;
        private readonly ILogger<OrdemServicoServico> _logger;
        public OrdemServicoServico(ILogger<OrdemServicoServico> logger, XptoContext xptoContext)
        {
            _logger = logger;
            _xptoContext = xptoContext;
        }

        public void Excluir(long id)
        {
            OrdemServico ordemServico = _xptoContext.OrdensServico.FirstOrDefault(x => x.Id == id);
            if(ordemServico != null)
            {
                _xptoContext.OrdensServico.Remove(ordemServico);
                _xptoContext.SaveChanges();
            }
        }

        public void Editar(OrdemServico ordemServico)
        {
            ValidarOrdemServico(ordemServico);
            _xptoContext.OrdensServico.Update(ordemServico);
            _xptoContext.SaveChanges();
        }

        public void Inserir(OrdemServico ordemServico)
        {
            ValidarOrdemServico(ordemServico);
            _xptoContext.OrdensServico.Add(ordemServico);
            _xptoContext.SaveChanges();
        }

        public IEnumerable<OrdemServico> Listar()
        {
            return _xptoContext.OrdensServico
                .Include(x => x.Cliente)
                .Include(x => x.PrestadorServico)
                .ToList();
        }

        public OrdemServico RecuperarPorNumero(string numero)
        {
            return _xptoContext.OrdensServico.FirstOrDefault(x => x.Numero == numero);
        }

        public OrdemServico Recuperar(int id)
        {
            return _xptoContext.OrdensServico
                 .Include(x => x.Cliente)
                 .Include(x => x.PrestadorServico)
                 .FirstOrDefault(x => x.Id == id);
        }

        private void ValidarOrdemServico(OrdemServico ordemServico)
        {
            if(ordemServico.DataExecucao == DateTime.MinValue || ordemServico.DataExecucao < DateTime.Now)
            {
                throw new ArgumentOutOfRangeException("Data de execução inválida!");
            }
            ordemServico.Titulo = ordemServico.Titulo.ToLower();

            if(ordemServico.Valor <= 0)
            {
                throw new ArgumentOutOfRangeException("Valor do serviço inválido!");
            }

        }
    }
}
