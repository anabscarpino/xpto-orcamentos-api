﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xpto.Entidades;
using Xpto.Repositorios;
using Xpto.Servicos.Interfaces;

namespace Xpto.Servicos
{
    public class PrestadorServicoServico : IPrestadorServicoServico
    {
        private readonly XptoContext _xptoContext;

        public PrestadorServicoServico(XptoContext xptoContext)
        {
            _xptoContext = xptoContext;
        }
        public PrestadorServico Inserir(PrestadorServico prestador)
        {
            if (!String.IsNullOrEmpty(prestador.Nome)) { prestador.Nome.ToLower(); }
            if (!String.IsNullOrEmpty(prestador.CpfCnpj)) { prestador.CpfCnpj = prestador.CpfCnpj.Replace(".", "").Replace("-", ""); }

            if (!CpfValido(prestador.CpfCnpj))
            {
                prestador.DocumentoInvalido = true;
            }
            _xptoContext.PrestadoresServico.Add(prestador);
            _xptoContext.SaveChanges();
            return prestador;
        }

        public PrestadorServico Recuperar(string cnpj)
        {
            return _xptoContext.PrestadoresServico.FirstOrDefault(x => x.CpfCnpj == cnpj);
        }

        private bool CpfValido(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;

            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }
    }
}
