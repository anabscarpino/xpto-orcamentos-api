﻿using Xpto.Entidades;

namespace Xpto.Servicos.Interfaces
{
    public interface IClienteServico
    {
        Cliente Recuperar(string cpf);
        Cliente Inserir(Cliente cliente);
    }
}
