﻿using Xpto.Entidades;

namespace Xpto.Servicos.Interfaces
{
    public interface IPrestadorServicoServico
    {
        PrestadorServico Recuperar(string cnpj);
        PrestadorServico Inserir(PrestadorServico prestador);
    }
}
