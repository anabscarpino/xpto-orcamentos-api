﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xpto.Entidades;

namespace Xpto.Servicos.Interfaces
{
    public interface IOrdemServicoServico
    {
        IEnumerable<OrdemServico> Listar();
        OrdemServico RecuperarPorNumero(string numero);
        OrdemServico Recuperar(int id);
        void Excluir(long id);
        void Editar(OrdemServico ordemServico);
        void Inserir(OrdemServico ordemServico);
    }
}
