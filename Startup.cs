using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Xpto.Aplicacoes;
using Xpto.Aplicacoes.Interfaces;
using Xpto.Entidades;
using Xpto.Models.Requests;
using Xpto.Models.Responses;
using Xpto.Repositorios;
using Xpto.Servicos;
using Xpto.Servicos.Interfaces;

namespace Xpto
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<OrdemServicoRequest, OrdemServico>();
                cfg.CreateMap<OrdemServico, OrdemServicoResponse>();

                cfg.CreateMap<OrdemServico, OrdemServicoResponse>()
                .ForMember(dest => dest.NomeCliente, opt => opt.MapFrom(src => src.Cliente.Nome))
                .ForMember(dest => dest.CnpjCliente, opt => opt.MapFrom(src => src.Cliente.CpfCnpj))
                .ForMember(dest => dest.NomePrestadorServico, opt => opt.MapFrom(src => src.PrestadorServico.Nome))
                .ForMember(dest => dest.CpfPrestadorServico, opt => opt.MapFrom(src => src.PrestadorServico.CpfCnpj));

            });
            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);
            services.AddMvc();
            services.AddEntityFrameworkNpgsql().AddDbContext<XptoContext>(options => options.UseNpgsql(Configuration.GetConnectionString("XptoDB")));
            services.AddScoped<IOrdemServicoAppServico, OrdemServicoAppServico>();
            services.AddScoped<IOrdemServicoServico, OrdemServicoServico>();
            services.AddScoped<IClienteServico, ClienteServico>();
            services.AddScoped<IPrestadorServicoServico, PrestadorServicoServico>();
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            }) ;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseRouting();
            
            app.UseAuthorization();
            app.UseCors("CorsPolicy");
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            //app.UseHttpsRedirection();
        }
    }
}
